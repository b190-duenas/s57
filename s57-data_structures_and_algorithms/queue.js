let collection = [];
// Write the queue functions below.

function print() {
    return collection;
}

function enqueue(add) {
    collection[collection.length]=add;
    return collection;
}

function dequeue(){
    for (let i = 0; i < collection.length; i++){
        collection[i] = collection[i+1];
    }
    collection.length = collection.length - 1;
    return collection;
}

function front() {
    return collection[0];
}

function size() {
    return collection.length;
}

function isEmpty(){
    if (collection.length >= 0){
        return false;
    } else {
        return true;
    }
}
module.exports = {
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};